﻿using System;

namespace TimesheetAPI.Data.Models
{
    public class Entry
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public string Title { get; set; }

        public int Hours { get; set; }
    }
}