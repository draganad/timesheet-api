﻿using System;
using System.Collections.Generic;
using TimesheetAPI.Data.Models;

namespace TimesheetAPI.Data
{
    public interface ITimesheetRepository
    {
        IEnumerable<Entry> Get(DateTime date);
        bool? Add(Entry entry);
    }
}
