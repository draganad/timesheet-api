﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using TimesheetAPI.Data.Models;

namespace TimesheetAPI.Data
{
    public class TimesheetRepository : ITimesheetRepository
    {
        private string _connectionString;
        private int _maxHours;

        public TimesheetRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["defaultConnection"].ConnectionString;
            _maxHours = int.Parse(ConfigurationManager.AppSettings["maxHours"]);
        }

        // GET api/entries
        public IEnumerable<Entry> Get(DateTime date)
        {
            const string queryString = "SELECT Date, Title, Hours, Id from dbo.Timesheet WHERE Date = @Date";

            List<Entry> entries = new List<Entry>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);

                try
                {
                    connection.Open();
                    command.Parameters.Add("@Date", SqlDbType.VarChar, 50).Value = date;
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Entry tmp = new Entry();
                        tmp.Date = (DateTime)reader[0];
                        tmp.Title = (string)reader[1];
                        tmp.Hours = (int)reader[2];
                        tmp.Id = (int)reader[3];

                        entries.Add(tmp);
                    }
                    reader.Close();
                    connection.Close();
                    return entries;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return entries;
        }

        // POST api/entries
        public bool? Add(Entry entry)
        {
            int hoursSum = GetHoursSum(entry.Date);
            if (_maxHours - hoursSum < entry.Hours)
            {
                return null;
            }

            const string queryString = "INSERT INTO dbo.Timesheet (Date, Title, Hours) VALUES (@Date, @Title, @Hours)";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);

                try
                {
                    command.Parameters.Add("@Date", SqlDbType.VarChar, 50).Value = entry.Date;
                    command.Parameters.Add("@Title", SqlDbType.VarChar, 50).Value = entry.Title;
                    command.Parameters.Add("@Hours", SqlDbType.VarChar, 50).Value = entry.Hours;
                    connection.Open();

                    command.ExecuteNonQuery();

                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return false;
        }

        private int GetHoursSum(DateTime date)
        {
            const string queryString = "SELECT SUM(Hours) as HoursSum from dbo.Timesheet WHERE Date = @Date";   

            int hoursSum = 0;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);

                try
                {
                    connection.Open();
                    command.Parameters.Add("@Date", SqlDbType.VarChar, 50).Value = date;
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        hoursSum = (int)reader[0];
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return hoursSum;
        }
    }
}
