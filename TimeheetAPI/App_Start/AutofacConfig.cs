﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using System.Web.Http;
using TimesheetAPI.Controllers;
using TimesheetAPI.Data;

namespace TimesheetAPI
{
    public static class AutofacConfig
    {
        public static void RegisterComponents()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<TimesheetRepository>().As<ITimesheetRepository>();
            builder.RegisterType<EntriesController>();
            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);

            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }

    }
}