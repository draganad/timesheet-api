﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using TimesheetAPI.Data;
using TimesheetAPI.Data.Models;

namespace TimesheetAPI.Controllers
{
    public class EntriesController : ApiController
    {
        private ITimesheetRepository _timesheeetRep;

        public EntriesController(ITimesheetRepository timesheeetRep)
        {
            _timesheeetRep = timesheeetRep;
        }

        // GET api/entries/date
        [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
        public IEnumerable<Entry> Get(DateTime date)
        {
            return _timesheeetRep.Get(date);
        }

        // POST api/entries
        [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
        public bool Post(Entry entry)
        {
            var res = _timesheeetRep.Add(entry);

            if (res == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            return (bool)res;
        }
    }
}
