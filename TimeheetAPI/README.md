# VEGA Timesheet API

## Setup
Open `TimesheetAPI.sln` in Visual Studio, then right click on solution and choose *Restore NuGet Packages*. After packages are restored, build the project.

Open SQL Server Managment Studio and connect to **SQLEXPRESS server**, use Windows Authentication. Right click on *Databases* > *Attach* > `timesheet.mdf`.

## Configuration
In `Web.config` setup your connection string for database:

```xml
<connectionStrings>
    <add name="defaultConnection" connectionString="Server=localhost\SQLEXPRESS;Database=timesheet;Integrated Security=True" />
</connectionStrings>
```

and specify maximum hours per day in `<appSettings>`:

```xml
<add key="maxHours" value="10" />
```

## Endpoints

The project contains two endpoints:

### GET /api/entries
Fetches task list for particular date (default today), optional parametar `date`
#### URL
```
http://localhost:62850/api/entries?date=2019-01-24
```
#### Response:
```json
[
    {
        "Id": 14,
        "Date": "2019-01-24T00:00:00",
        "Title": "Zurka",
        "Hours": 4
    },
    {
        "Id": 21,
        "Date": "2019-01-24T00:00:00",
        "Title": "Zurka",
        "Hours": 4
    }
]
```
### POST /api/entries
Adds new task 
#### URL
```
http://localhost:62850/api/entries?date=2019-01-24
```
#### Request Body
```json
{
    "Date": "2020-01-09T00:00:00",
    "Title": "Ispit",
    "Hours": 2
}
```